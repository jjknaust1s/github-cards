import React,{useState} from 'react';
import { Button, Card, ListGroup, ListGroupItem } from 'react-bootstrap'
import Farout from "./images/background.mp4"
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  const [user, setUser] = useState({})
  const [active,setActive] =useState(false)
  function handleToggle(){
    // https://api.github.com/users/jjknaust1s
    console.log('hello')
  
  fetch('https://api.github.com/users/jjknaust1s')
    .then((response) => {
  if(!response.ok){
    throw Error ('Error fetching')
  }

  return response.json()
  })
  .then((allData) => {
   setUser(allData)
   console.log(allData)
  })
  .catch((err) => {
  throw Error (err.message)
  })
  if(active===false){
    setActive(true)
  }else{
    setActive(false)
  }
  
  }

  return (
    <>
    <video autoPlay loop muted
    style={{
      position:'absolute',
      width: '100%',
      height:'100%',
      // trasnform:'translate (-50%, -50%)',
      objectFit: 'cover',
      // zIndex:'-1'


    }}>
      <source src={Farout} type='video/mp4'/>
    </video>
    <Button 
    style={{position:'relative'}} 
    variant="primary" onClick= {handleToggle}>Click Me</Button>
    {active&& 
    <Card style={{ width: '18rem' }} style={{backgroundImage: 'linear-gradient(to bottom, #fbc2eb 0%, #a6c1ee 100%)'}}>
      <Card.Title>Profile Picture</Card.Title>
      <Card.Img variant="top" src={user.avatar_url} />
      <Card.Body>
      <ListGroup>
      <ListGroupItem style={{backgroundColor:'rgb(246, 244, 138)'}}><Card.Title>Name:{user.name}</Card.Title></ListGroupItem>
      <br/>
      <ListGroupItem style={{backgroundColor:'rgb(246, 244, 138)'}}><Card.Text>Username: {user.login}</Card.Text></ListGroupItem>
      <br/>
      <ListGroupItem style={{backgroundColor:'rgb(246, 244, 138)'}}><Card.Text>Bio: {user.bio}</Card.Text></ListGroupItem>
      </ListGroup>
     </Card.Body>
      </Card>}
    </>
 
  );
}

export default App;
